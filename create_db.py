from project import db
from project.models import User

# # Create the db and the table
db.create_all()
#
# # insert data
# db.session.add(User('adminadmin', 'ad@min.com', 'adminadmin', 'admin'))
# db.session.add(User('test', 'test@test.com', 'test', 'user'))
#
# # Commit changes
db.session.commit()
