# README #

This is a web site where it's users store and share their vacation pictures and experiences.

### What is this repository for? ###

* Quick summary

The main repositary for the Vacation Diary

### How do I get set up? ###
* Create your virtual environment

```bash
$ virtualenv venv

```
* Clone the repository
```bash
$ git clone git@bitbucket.org:vacationdigital/vacationdiary.git
```
* Activate your virtual environment
```bash
$ source venv/Script/activate
```
* Install the dependencies
```bash
$ pip install -r requirements.txt
```