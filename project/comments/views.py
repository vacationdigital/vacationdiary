from flask import flash, redirect, render_template, request, session, url_for, Blueprint
from datetime import datetime
from project.models import Comments
from project import db

from project.funcs.general_funcs import redirect_dest

comments_blueprint = Blueprint('comments', __name__)


@comments_blueprint.route('/post_comment/<int:vacation_id>', methods=['POST'])
def log_comment(vacation_id):
    try:
        new_comment = Comments(
            user_name=session['name'],
            text=request.form['comment'],
            date=datetime.now(),
            vacation_id=vacation_id,
        )

        db.session.add(new_comment)
        db.session.commit()
    except KeyError:
        flash('You must log-in to leave a comment')
        return redirect(url_for('user.login'))
    return redirect_dest(fallback=url_for('myVacation.main'))
