from functools import wraps
from flask import flash, redirect, render_template, request, session, url_for, Blueprint
from project import db, bcrypt, app
from project.models import User
from sqlalchemy.exc import IntegrityError
from project.funcs.general_funcs import _hash_image_name, upload_to_s3, delete_from_s3, redirect_dest
from project.funcs.image_manipulation import resize_img
from wtforms.validators import Length

from project.user.forms import LoginForm, RegisterForm, ProfileForm

user_blueprint = Blueprint('users', __name__)


def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            flash('You need to log in !!')
            return redirect(url_for('users.login'))

    return wrap


def _is_image(image):
    if image.filename != '':
        return True
    return False


@user_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    form = LoginForm(request.form)
    if request.args.get('next') is not None:
        session['dest'] = request.args.get('next')
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(name=request.form['name']).first()
            if user is not None and bcrypt.check_password_hash(user.password, request.form['password']):
                session['logged_in'] = True
                session['user_id'] = user.id
                session['role'] = user.role
                session['name'] = user.name
                flash('Welcome')
                if 'dest' not in session or session['dest'] is None:
                    return redirect_dest(fallback=url_for('myVacation.main'))
                return redirect_dest(fallback=url_for('myVacation.main'), target=session['dest'])
            else:
                error = 'Invalid Username or Password.'
    return render_template('login.html', form=form, error=error)


@user_blueprint.route('/register/', methods=['GET', 'POST'])
def register():
    error = None
    form = RegisterForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            new_user = User(
                form.name.data,
                form.email.data,
                bcrypt.generate_password_hash(form.password.data),
            )
            try:
                db.session.add(new_user)
                db.session.commit()
                flash('Thanks for registering. Please Login.')
                return redirect(url_for('users.login'))
            except IntegrityError:
                error = 'That username and/or email already exist.'
                return render_template('register.html', form=form, error=error)

    return render_template('register.html', form=form, error=error)


@user_blueprint.route('/logout/')
@login_required
def logout():
    flash('Goodbye ! {}'.format(session['name'].upper()))
    session.pop('logged_in', None)
    session.pop('user_id', None)
    session.pop('name', None)
    session.pop('role', None)
    session.pop('dest', None)
    return redirect(url_for('main.main'))


@user_blueprint.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    error = ''
    form = ProfileForm()

    if request.method == 'POST':

        # if form.validate_on_submit():
        if _is_image(request.files['photo']):
            uploaded_images = request.files.getlist('photo')
            for image in uploaded_images:
                image_size = 'profile'
                processed_image_name = _hash_image_name(image, image_size)
                directory = '/'.join([session['name'], processed_image_name])
                thumbnail_img = resize_img(image, image_size)
                upload_to_s3(str(directory), thumbnail_img, app.config['PROFILE_BUCKET_NAME'])
                user = User.query.filter_by(id=session['user_id']).first()
                delete_from_s3(app.config['PROFILE_BUCKET_NAME'], session['name'] + '\\' + user.user_image_name)
                user.user_image_name = processed_image_name
                db.session.commit()
                request.files = None

        if form.password.data:
            if form.password.data != form.confirm.data:
                error = 'Password Does Not Match !!'
            elif not form.password.validate(form, extra_validators=[Length(min=5, max=25)]):
                    error = 'Password Must be longer than 5 characters'
            else:
                new_password = bcrypt.generate_password_hash(form.password.data)
                user = User.query.filter_by(id=session['user_id']).first()
                user.password = new_password
                db.session.commit()
                flash('Please Log-in Again.')
                return redirect(url_for('users.logout'))
        if form.email.data:
            user = User.query.filter_by(id=session['user_id']).first()
            user.email = form.email.data
            db.session.commit()

    profile_info = {
        'name': session['name'],
        'email': User.query.filter_by(id=session['user_id']).first().email,
        'profile_image': User.query.filter_by(id=session['user_id']).first().user_image_name,
    }

    return render_template('profile.html', form=form, error=error, profile_info=profile_info)
