from flask_wtf import FlaskForm
from flask_uploads import UploadSet, IMAGES
from wtforms import StringField, PasswordField
from wtforms.fields.html5 import EmailField
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import DataRequired, Length, EqualTo, Email

images = UploadSet('images', IMAGES)


class RegisterForm(FlaskForm):
    name = StringField('Username', validators=[DataRequired(), Length(min=5, max=25)])
    email = StringField('Email', validators=[DataRequired(), Email(), Length(min=6, max=40)])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=5, max=40)])
    confirm = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])


class LoginForm(FlaskForm):
    name = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


class ProfileForm(FlaskForm):
    photo = FileField('Select Images', validators=[
        # FileRequired(),
        FileAllowed(['jpg', 'png'], 'Images only')],
                      render_kw={'multiple': False, }
                      )
    name = StringField('Username')
    password = PasswordField('Password')
    confirm = PasswordField('Repeat Password')
    email = EmailField('Email')
