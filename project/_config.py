import os

# Grab the folder there the script lives
basedir = os.path.abspath(os.path.dirname(__file__))

DATABASE = 'dev.db'
CSRF_ENABLED = True
SECRET_KEY = os.urandom(24)
DEBUG = True

SQLALCHEMY_TRACK_MODIFICATIONS = False

# define the full path for the db
DATABASE_PATH = os.path.join(basedir, DATABASE)

# the db uri
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE_PATH

# ALLOWED EXTENSIONS
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

# AWS S3
ACCESS_KEY_ID = 'AKIAJLETHVDCR4VMGRJQ'
SECRET_ACCESS_KEY = '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'
IMAGES_BUCKET_NAME = 'vacationimages'
PROFILE_BUCKET_NAME = 'vacationprofileimages'

# AWS IMAGE
AWS_IMG_SRC = 'https://s3.amazonaws.com/vacationimages/static/image'
AWS_PROFILE_SRC = 'https://s3.amazonaws.com/vacationprofileimages'
AWS_HOMEPAGE_IMAGE_SRC = 'vd-homepage-image'

# upload folders
IMG_UPLOAD_FOLDER = 'static\image\\'
