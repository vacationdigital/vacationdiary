from flask import flash, redirect, render_template, request, session, url_for, Blueprint
from project.models import User
from project import db, bcrypt

main_blueprint = Blueprint('main', __name__)


@main_blueprint.route('/', methods=['GET', 'POST'])
def main():
    return render_template('index.html')

