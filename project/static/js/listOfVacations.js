
var normalize_vac_name =  function (){
    var vac_name = $('.vac_name');
    vac_name.each(function () {
        var temp = $(this).text().toUpperCase().replace(/-/g, ' ');
        $(this).text(temp);
    });
};

$(function () {
    var $left_menu = $('.left-menu');

    if ($left_menu.length <= 1) {
        var $main = $('.col-md-8');
        $main.css({
            'width': '100%',
            'padding': 0
        });
    }
    normalize_vac_name();

    function add_more_vacations(data) {
        var img_src = 'https://s3.amazonaws.com/vacationimages/static/image/';
        $.each(JSON.parse(data), function (index, value) {
            var container = $('<div class="logged_vacation_container"/>');
            var title = $('<a class="vac_name"/>');
            var vac_img = $('<a class="vac_list_img"><img class="vacation_list_img"></a>');
            var desc = $('<p class="vacation_list_desc"/>');
            var EOL_hr = $('<hr class="between_logged_vacations">');

            container.append(title);
            container.append(vac_img);
            container.append(desc);
            container.append(EOL_hr);

            $(container).insertAfter($('.logged_vacation_container:last'));

            $(".vac_name:last").attr('href', '/myVacation/'+ value[0]).text(value[0]);
            $('.vac_list_img:last').attr('href', '/myVacation/'+ value[0]);
            $('.vacation_list_img:last').attr('src', img_src + value[3] + '/' + value[0] + '/' + value[2]);
            $('.vacation_list_desc:last').text(value[1]);

            normalize_vac_name();
            if (!value[4]){
                $('#load_more').css('display', 'none');
            }
        })
    }

    $('#load_more').click(function (e) {
        e.preventDefault();

        var total_vacation_displayed = $('.vac_name');

        $.ajax({
            type: 'POST',
            url: "/get_more_vacation",
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify({
                'total_vacation_displayed': total_vacation_displayed
            }),
            success: function (result) {
                add_more_vacations(result)
            },
            error: function (result) {
                console.log(result)
            }
        })
    });
});