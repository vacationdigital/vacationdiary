/**
 * DodosTextCounter - jQuery plugin for text limit for input or textarea. The counter shows how many chars are remaining.
 *						It will also disable the user from entering more characters after s/he reaches the maximum
 * Written by Ying Zhang aka Dodo
 * http://pure-essence.net/2008/05/30/dodos-text-counterdodos-text-counter/
 */

jQuery.fn.dodosTextCounter = function(max, options) {
    // if the counter display doesn't exist, the script will attempt to create it
    options = $.extend({
        counterDisplayElement: "span",					// tag for the counter display
        counterDisplayClass: "dodosTextCounterDisplay",	// class for the counter display
        addLineBreak: true								// whether to add <br /> after the input element before the counter display
    }, options);

    $(this).each(function(i) {
        updateCounter(this, max, options, i);
        $(this).keyup(function() {
            updateCounter(this, max, options, i);
            return this;
        });
    });
    return this;
};

function updateCounter(input, max, options, index) {
    var currentLength = 0;
    var val = $(input).val();
    if(val) {
        currentLength = val.length;
    }
    if(currentLength > max) {
        $(input).val(val.substring(0, max));
    } else {
        var charLeft = max - currentLength;
        var counterDisplay = options.counterDisplayElement + "." + options.counterDisplayClass + ":eq("+index+")";
        var createNew = $(counterDisplay).length == 0;
        if(createNew) {
            var element = document.createElement(options.counterDisplayElement);
            if(options.counterDisplayElement == 'input') {
                $(element).val(charLeft.toString());
            } else {
                $(element).html(charLeft.toString());
            }
            $(element).addClass(options.counterDisplayClass).insertAfter($(input));
            if(options.addLineBreak) {
                $(input).after("<br />");
            }
        } else {
            if(options.counterDisplayElement == 'input') {
                $(counterDisplay).val(charLeft.toString());
            } else {
                $(counterDisplay).html(charLeft.toString());
            }
        }

    }
}
