function display_comments(logged, comments, vacation_id) {

    var comments_wrapper = $('.comments_wrapper');

    if (logged == false) {
        $('.submit_comment').attr('disabled', true);
        $('.submit_comment').css('cursor', 'not-allowed');
        var target = location.pathname;
        $('.comment_notification').append('Please ' + '<a>login</a>' + ' to leave a comment');
        $('.comment_notification a').attr('href', '/login?next=' + target);
        $('.log_comments_wrapper textarea').css({
            'color': '#aeaeae'
        });
        $('.log_comments_wrapper textarea').attr('disabled', 'disabled')
    }
    var counter = 0;
    for (var i = comments.length - 1; i >= 0; i--) {
        counter += 1
        if (counter == 10){break;}
        var comments_body_div = $("<div class='comments_body' />");
        var by_line = $('<span class="byline">');
        var commenter = $("<span class='comments_by_line' />");
        var comments_date = $("<span class='comment_date'>");

        comments_body_div.append('<p class="comment_area_text">' + comments[i][1] + '</p>');
        commenter.append('by ' + '<b>' + comments[i][0] + '</b>');
        comments_date.append(' - ' + comments[i][2]);

        comments_wrapper.append(comments_body_div);
        by_line.append(commenter);
        by_line.append(comments_date);
        comments_wrapper.append(by_line);
        comments_wrapper.append('<hr class="EOComment">');

        comments_wrapper.css({
            'width': '80%',
            'margin': 'auto',
            'display': 'block'
        });

        by_line.css({
            'float': 'right'
        });

        comments_body_div.css({
            'border': '1px solid #aeaeae',
            'min-height': '80px',
            'width': '100%',
            'margin': 'auto',
            'display': 'block',
            'background-color': 'white'
        });
    }

    jQuery(function ($) {
        $("textarea.comments").dodosTextCounter(100, {
            counterDisplayElement: "input",
            counterDisplayClass: "textareaCounter"
        });
    });
}
