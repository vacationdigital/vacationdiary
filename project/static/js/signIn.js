$(function (e) {
    console.info('Registery loaded');

    var exit_modal = function(){
        modal.animate({
            left: '-=' + $(window).width(),
            opacity: 'toggle'
        }, 500, function () {
            window.location.replace('/');
        });
    };

// Get the modal
    var modal = $('#pop-up-modal');

// Get the button that opens the modal
    var btn = $("#singup");

// Get the .close element that closes the modal
    var regClose = $('.pop-up-close');

// When the user clicks the button, open the modal
    btn.click(function () {
        modal.style.display = "block";
    });

// When the user clicks on <span> (x), close the modal
    regClose.click(function () {
        exit_modal();
    });

// When the user clicks anywhere outside of the modal, close it
    $(window).click(function (event) {
        if (event.target == modal[0]) {
            exit_modal();
        }
    });
    $('#name').focus();
});

