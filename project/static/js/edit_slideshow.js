$(function () {

    $('.delete_img').click(function () {
        var img_id = $(this).attr('class');
        img_id = img_id.split(" ")[1];
        img_id = img_id.replace('.', '\\.');
        try{
            $(".checkbox_" + img_id).attr("value", "delete_" + img_id);
            $(this).parent().closest('div').hide('slow');

        }
        catch (err){
            console.log(err);
        }
    });
    $(":file").change(function(){
        $('#photo').css("background-color", "#0477BF");
    });
    // $(":file").css("background-color", "#aeaeae");
    $(function(){
        $("#location").geocomplete();
    });
    var vac_name = $('.vac_name');
    var temp = vac_name.text().toUpperCase().replace(/-/g, ' ');
    $(vac_name).text(temp);
});
