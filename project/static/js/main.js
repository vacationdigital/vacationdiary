$(function (e) {
    var $left_menu = $('.left-menu');

    if ($left_menu.length <= 1) {
        var $main = $('.col-md-8');
        $main.css({
            'width': '100%',
            'padding': 0
        });
    }
    var vac_name = $('.vac_name');
    vac_name.each(function () {
        var temp = $(this).text().toUpperCase().replace(/-/g, ' ');
        $(this).text(temp);
    });

    // $.ajax({
    //     type: 'GET',
    //     url: "/get_homepage_bg",
    //     contentType: 'application/json;charset=UTF-8',
    //
    //     success: function (result) {
    //         set_bg_image(result)
    //     },
    //     error: function (result) {
    //         console.log(result)
    //     }
    // });
    //
    // function set_bg_image(image_name) {
    //     var image_url = 'https://s3.amazonaws.com/vd-homepage-image/' + image_name;
    //     $('.image-bg-fluid-height').css(
    //         'background', "url('" + image_url + "') no-repeat center center scroll");
    // }

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    if ($("#location").length) {
        $(function () {
            $("#location").geocomplete();
        });
    }

    var $root = $('html, body');
    $('.page-scroll').click(function () {

        $root.animate({
            scrollTop: $($.attr(this, 'href')).offset().top

        }, 500);

        return false;
    });
});
