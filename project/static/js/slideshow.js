function vacation_slideshow(photo_names, caption){
    var slideshow_container_div = $(".vacation_slideshow_container");

    function getImagePath(){
        return photo_names[k];
    }

    for (var k = 0; k < photo_names.length; k++){
        var mySlides_div = $('<div class="mySlides img_fade"></div>');
        var numberText_div = $('<div class="numbertext"></div>');
        var img_tag = $('<img class="vac_img" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >');
        var caption_div = $('<div class="caption_text"></div>');
        var normalized_caption = caption.toUpperCase().replace(/-/g, ' ');

        $(numberText_div).text(k+1 + '/' + photo_names.length);
        $(caption_div).text(normalized_caption);
        mySlides_div.append(numberText_div);
        mySlides_div.append(img_tag);
        mySlides_div.append(caption_div);
        slideshow_container_div.append(mySlides_div);

        $(".vac_img:last").attr('src', getImagePath(k));
    }

//    Prev/next buttons
    var img_prev = $('<a class="slideshow_prev">&#10094;</a>');
    var img_next = $('<a class="slideshow_next">&#10095;</a>');

    slideshow_container_div.append(img_prev);
    slideshow_container_div.append(img_next);

    $(img_prev).click(function(){
        plusSlides(-1)
    });

    $(img_next).click(function(){
        plusSlides(1)
    });

//    Slideshow Func
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function showSlides(n){
        var i;
        var slides = $(".mySlides");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}

        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slides[slideIndex-1].style.display = "block";
    }

    var target = location.pathname;
    $('#edit_slideshow a').attr('href', '/edit?next=' + target);
}