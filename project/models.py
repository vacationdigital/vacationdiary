from project import db
import datetime


# ONE to MANY Relation DB
# ## MANY = ForeignKey
# ## ONE = relationship
class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    role = db.Column(db.String, default='user')
    user_image_name = db.Column(db.String, nullable=True)

    vacation = db.relationship('Vacation', backref='users')

    def __init__(self, name=None, email=None, password=None, role=None, user_image_name=None):
        self.name = name
        self.email = email
        self.password = password
        self.role = role
        self.user_image_name = user_image_name

    def __repr__(self):
        return '{}'.format(self.name)


class Vacation(db.Model):
    __tablename__ = "vacation"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String, nullable=False)
    location = db.Column(db.String, nullable=False)
    with_who = db.Column(db.String, nullable=True)
    description = db.Column(db.String, nullable=True)
    public = db.Column(db.BOOLEAN, nullable=False)
    log_date = db.Column(db.DATETIME(timezone=True), nullable=False)
    vacation_date = db.Column(db.DATETIME(timezone=True), nullable=False)

    # vacation_photos = db.relationship("VacationPhoto", backref="vacation")

    user_id = db.Column(db.ForeignKey('users.id'))

    def __init__(self, name, location, with_who, description, user_id, public, log_date, vacation_date):
        self.name = name
        self.location = location
        self.with_who = with_who
        self.description = description
        self.user_id = user_id
        self.public = public
        self.log_date = log_date
        self.vacation_date = vacation_date

    # def __repr__(self):
    #     return '{} {} {} {} {} {} {} {}'.format(
    #         self.name,
    #         self.location,
    #         self.with_who,
    #         self.description,
    #         self.user_id,
    #         self.public,
    #         self.log_date,
    #         self.vacation_date
    #     )


class VacationPhoto(db.Model):
    __tablename__ = "vacationPhotos"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    photo_name = db.Column(db.String)
    vacation_id = db.Column(db.Integer)

    def __init__(self, photo_name, vacation_id):
        self.photo_name = photo_name
        self.vacation_id = vacation_id

    def __repr__(self):
        return '{}'.format(self.photo_name)


class Comments(db.Model):
    __tablename__ = "comments"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_name = db.Column(db.Integer, nullable=False)
    text = db.Column(db.String, nullable=False)
    date = db.Column(db.DATETIME(timezone=True), nullable=False)
    vacation_id = db.Column(db.Integer, nullable=False)

    def __init__(self, user_name, text, date, vacation_id):
        self.user_name = user_name
        self.text = text
        self.date = date
        self.vacation_id = vacation_id
