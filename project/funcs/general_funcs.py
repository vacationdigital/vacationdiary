import traceback

from project.models import Vacation, User, VacationPhoto
from project import app
from werkzeug.utils import secure_filename
from project import bcrypt
from flask import request, redirect
import time
import boto3


def recent_posts():
    data_list = []
    vacation = Vacation.query.all()
    for i in vacation:
        data = {}
        if i.public is True:
            data['id'] = i.id
            data['name_original'] = i.name.replace('-', ' ').upper()
            data['name_slugged'] = i.name
            data['location'] = i.location
            data['with_who'] = i.with_who
            data['description'] = i.description
            data['log_date'] = i.log_date
            data['vacation_date'] = i.vacation_date
            data['user_name'] = User.query.filter_by(id=i.user_id).first()
            data['main_image'] = VacationPhoto.query.filter_by(vacation_id=i.id).first()

            data_list.append(data)
    return data_list


def _conn_to_s3():
    print('Making connection to S3')
    try:
        s3 = boto3.client('s3',
                          aws_access_key_id=app.config['ACCESS_KEY_ID'],
                          aws_secret_access_key=app.config['SECRET_ACCESS_KEY'])
        return s3
    except Exception as err:
        print(err)
        traceback.print_exc()


def upload_to_s3(s3_upload_folder, file, bucket_name):
    try:
        s3 = _conn_to_s3()
        fixed_s3_upload_folder = s3_upload_folder.replace('\\', '/')

        print('Uploading Images')
        s3.upload_fileobj(file, bucket_name, fixed_s3_upload_folder)
    except Exception as e:
        print(e)


def download_from_s3():
    try:
        s3 = _conn_to_s3()

    except Exception as e:
        print(e)


def delete_from_s3(bucket_name, file_path):
    try:
        s3 = _conn_to_s3()
        fixed_path = file_path.replace('\\', '/')
        s3.delete_object(Bucket=bucket_name, Key=fixed_path)
    except Exception as e:
        print(e)


def _hash_image_name(image, image_size):
    filename = secure_filename(image.filename)
    extension = filename.split('.')[1]
    filename = filename.split('.')[0]
    filename = str(filename) + str(time.time())
    hash_file_name = bcrypt.generate_password_hash(filename).decode('utf-8') + '_' + image_size
    filename = "".join([c if c.isalnum() else "" for c in hash_file_name])
    return filename + '.' + str(extension)


def redirect_dest(fallback, target=None):
    dest = request.args.get('next')
    if dest is None:
        dest = target
        if target is None:
            dest = fallback
    try:
        dest_url = dest
    except:
        return redirect(fallback)
    return redirect(dest_url)


app.jinja_env.globals.update(recent_posts=recent_posts)
