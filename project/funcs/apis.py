import json
import random

from flask import request, session

from project.models import User, Vacation, VacationPhoto
from project.funcs.general_funcs import _conn_to_s3
from project import app


@app.route('/get_more_vacation', methods=['GET', 'POST'])
def get_more_apis():
    is_more = True
    if request.method == "POST":
        n = request.json['total_vacation_displayed']
        start_of_range = n['length']
        end_of_range = start_of_range + 5

        list_of_vacation = []
        user_info = User.query.filter_by(name=session['name']).first()
        vacation = Vacation.query.filter_by(user_id=user_info.id)
        last_item = vacation[-1]
        print(last_item)

        for vac in vacation[start_of_range:end_of_range]:
            if vac.name == last_item.name:
                is_more = False
            vacation_img = VacationPhoto.query.filter_by(vacation_id=vac.id).first()
            list_of_vacation.append([vac.name,
                                     vac.description,
                                     vacation_img.photo_name,
                                     session['name'],
                                     is_more]
                                    )
        return json.dumps(list_of_vacation)


@app.route('/get_homepage_bg')
def homepage_bg_image():
    s3 = _conn_to_s3()
    homepage_bucket = s3.list_objects(Bucket=app.config['AWS_HOMEPAGE_IMAGE_SRC'])
    image_list = []

    for item in homepage_bucket['Contents']:
        for key, value in item.items():
            if key == 'Key' and value.endswith('.jpg'):
                image_list.append(value)

    return random.choice(image_list)
