from PIL import Image
from io import BytesIO


def resize_img(original_image, size):
    sizes = {
        'thumbnail': (128, 128),
        'profile': (400, 400),
        'medium': (640, 480),
        'large': (1024, 768)
    }

    im = Image.open(BytesIO(original_image.read()))
    img = im.copy()

    img.thumbnail(sizes[size], Image.ANTIALIAS)
    byte_io = BytesIO()
    img.save(byte_io, 'JPEG')
    byte_io.seek(0)
    return byte_io
