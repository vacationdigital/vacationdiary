import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt

app = Flask(__name__)
app.secret_key = os.urandom(24)
app.config.from_pyfile('_config.py')
bcrypt = Bcrypt(app)
db = SQLAlchemy(app)

from project.main.views import main_blueprint
from project.user.views import user_blueprint
from project.myVacation.views import myVacation_blueprint
from project.comments.views import comments_blueprint
from project.funcs import general_funcs  # ==> General Functions
from project.funcs import apis  # ==> APIs
from project.error import views  # ==> Error Handling

# # Register the blueprints
app.register_blueprint(main_blueprint)
app.register_blueprint(user_blueprint)
app.register_blueprint(myVacation_blueprint)
app.register_blueprint(comments_blueprint)
