from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from flask_uploads import UploadSet, IMAGES
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired, Length
from wtforms.fields.html5 import DateField

images = UploadSet('images', IMAGES)


class LogVacationForm(FlaskForm):
    vacation_name = StringField('Vacation Name', validators=[DataRequired(), Length(min=6, max=25)])
    location = StringField('Location', validators=[DataRequired()])
    with_who = StringField('With_Who')
    description = TextAreaField('Description', render_kw={
        'class': 'vacation_description',
        'rows': 10
    })
    photo = FileField('Select Images', validators=[
        # FileRequired(),
        FileAllowed(['jpg', 'png'], 'Images only')],
                      render_kw={'multiple': True,}
                      )
    when = DateField('when', validators=[DataRequired()])


class EditVacationFrom(FlaskForm):
    photo = FileField('Select Images', validators=[
        # FileRequired(),
        FileAllowed(['jpg', 'png'], 'Images only')],
                      render_kw={'multiple': True,}
                      )
    description = TextAreaField('Description', render_kw={
        'class': 'vacation_description',
        'rows': 10
    })
    location = StringField('Location', validators=[DataRequired()])
    with_who = StringField('With_Who')
