import os
import json
from datetime import datetime
from datetime import date
from functools import wraps

from flask import flash, redirect, render_template, request, session, url_for, Blueprint, abort
from slugify import slugify

from project import db, app
from project.models import Vacation, User, VacationPhoto, Comments
from project.myVacation.forms import LogVacationForm, EditVacationFrom

from project.funcs.general_funcs import _hash_image_name, upload_to_s3, delete_from_s3
from project.funcs.image_manipulation import resize_img

import traceback

myVacation_blueprint = Blueprint('myVacation', __name__)


def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            flash('You need to log in !!')
            return redirect(url_for('users.login'))

    return wrap


def _user_img_folder(form, file_name):
    username = session['name']
    try:
        vacation_name = slugify(form.vacation_name.data)
    except:
        vacation_name = form
    directory = os.path.join(username, vacation_name)
    directory = os.path.join(app.config['IMG_UPLOAD_FOLDER'], directory)
    return directory + '/' + file_name


def _get_date(form):
    year, month, day = request.form.get('when').split('-')
    vac_date = date(int(year), int(month), int(day))
    return vac_date


def _get_privacy_bool(form):
    privacy = False
    if request.form.get('public'):
        privacy = True
    return privacy


def _is_image():
    if request.files['photo'].filename != '':
        return True
    return False


def _save_vacation(form):
    vacation = Vacation(slugify(form.vacation_name.data),
                        form.location.data,
                        form.with_who.data,
                        form.description.data,
                        session['user_id'],
                        _get_privacy_bool(form),
                        datetime.utcnow(),
                        _get_date(form)
                        )
    db.session.add(vacation)
    db.session.commit()


def _save_photos(form, normalized_image_names):
    try:
        vac = db.session.query(Vacation).filter(Vacation.name == slugify(form.vacation_name.data)).one()
    except:
        vac = db.session.query(Vacation).filter(Vacation.name == form).one()
    print('Adding image names to the DB')
    for image_name in normalized_image_names:
        image = VacationPhoto(
            image_name,
            vac.id
        )
        db.session.add(image)
        db.session.commit()


@myVacation_blueprint.route('/myVacation', methods=['GET'])
@login_required
def main():
    return render_template('myVacation.html')


@myVacation_blueprint.route('/myVacation/<string:vacation_name>', methods=['GET'])
# @login_required
def vacation(vacation_name=None):
    photo_names = []
    all_comments = []
    logged = False
    owner = False
    session['dest'] = '/edit?next=/myVacation/' + vacation_name
    try:
        vacation_info = Vacation.query.filter_by(name=vacation_name).first()

        if 'logged_in' in session:
            logged = True
            if vacation_info.user_id == User.query.filter_by(name=session['name']).first().id:
                owner = True

        if vacation_info is None:
            abort(404)

        vacation_comments = Comments.query.filter_by(vacation_id=vacation_info.id).all()

        for comment in vacation_comments:
            all_comments.append([comment.user_name,
                                 comment.text,
                                 comment.date]
                                )

        image_data = VacationPhoto.query.filter_by(vacation_id=vacation_info.id).all()
        user_data = User.query.filter_by(id=vacation_info.user_id).first()

        for i in image_data:
            file = '/'.join([app.config['AWS_IMG_SRC'], user_data.name, vacation_info.name, i.photo_name])
            photo_names.append(file)
    except Exception as e:
        traceback.print_exc()
        abort(404)

    return render_template('vacation.html',
                           vacation=vacation_info,
                           photo_names=photo_names,
                           logged=logged,
                           comments=all_comments,
                           owner=owner
                           )


def _img_preparation(img_size, image, form):
    processed_image_name = _hash_image_name(image, img_size)
    large_image = resize_img(image, img_size)
    directory = _user_img_folder(form, processed_image_name)
    return processed_image_name, large_image, directory


@myVacation_blueprint.route('/myVacation/logVacation', methods=['GET', 'POST'])
@login_required
def log_vacation(vacation_name=None):
    error = None
    form = LogVacationForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                normalized_image_names = []
                if _is_image():
                    uploaded_images = request.files.getlist('photo')
                    for image in uploaded_images:
                        image_size = 'large'
                        processed_image_name, large_image, directory = _img_preparation(image_size, image, form)
                        upload_to_s3(str(directory), large_image, app.config['IMAGES_BUCKET_NAME'])
                        normalized_image_names.append(processed_image_name)

                _save_vacation(form)
                if _is_image():
                    _save_photos(form, normalized_image_names)

                flash('Vacation saved')
                return redirect(url_for('myVacation.log_vacation'))
            except Exception as e:
                traceback.print_exc()
        return redirect(url_for('myVacation.log_vacation'))
    return render_template('logVacation.html', form=form, error=error)


@myVacation_blueprint.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    form = EditVacationFrom()
    Qparam = request.args.get('next')
    target = Qparam.split('/')[2]

    vacation_info = Vacation.query.filter_by(name=target).first()

    user_name = session['name']
    images = VacationPhoto.query.filter_by(vacation_id=vacation_info.id).all()

    if vacation_info.user_id == User.query.filter_by(name=session['name']).first().id:
        if request.method == 'POST':
            # Delete the selected images
            for img_name in request.form.getlist('deleted'):
                if img_name.startswith('delete'):
                    img_name = img_name.replace('\\', '')
                    img_name = img_name.split('_')[1]
                    delete_from_s3(app.config['IMAGES_BUCKET_NAME'],
                                   'static\\image\\' + session['name'] + '\\' + vacation_info.name + '\\' + img_name)
                    VacationPhoto.query.filter_by(photo_name=img_name).delete()
                    db.session.commit()

            # Upload the new images
            if _is_image():
                normalized_image_names = []
                uploaded_images = request.files.getlist('photo')
                for image in uploaded_images:
                    image_size = 'large'
                    processed_image_name, large_image, directory = _img_preparation(image_size, image,
                                                                                    vacation_info.name)
                    upload_to_s3(str(directory), large_image, app.config['IMAGES_BUCKET_NAME'])
                    normalized_image_names.append(processed_image_name)

                if _is_image():
                    _save_photos(target, normalized_image_names)
                    print('Adding image names to the DB ... Done !!')

            # Update the new description
            if vacation_info.description != request.form['description']:
                vacation_info.description = request.form['description']
                db.session.commit()

            # Update Location
            if vacation_info.location != request.form['location']:
                vacation_info.location = request.form['location']
                db.session.commit()

            # Update with_who
            if vacation_info.with_who != request.form['with_who']:
                vacation_info.with_who = request.form['with_who']
                db.session.commit()

            flash('Update was Successful !!')
            print('Update was Successful !!')
            return redirect(url_for('myVacation.edit', next=Qparam))
    else:
        flash('Username Didn\'t Match !!!')
        return redirect(url_for('myVacation.vacation', vacation_name=target))
    return render_template('edit_slideshow.html',
                           image_list=images,
                           user_name=user_name,
                           vacation_info=vacation_info,
                           form=form
                           )


@myVacation_blueprint.route('/myVacation/loggedVacations', methods=['GET', 'POST'])
@login_required
def logged_vacations():
    is_more = True
    list_of_vacation = []
    user_info = User.query.filter_by(name=session['name']).first()
    vacation = Vacation.query.filter_by(user_id=user_info.id)
    last_item = vacation[-1]
    vacation_info = vacation.limit(5).all()
    last_selected = vacation_info[-1]

    if last_item == last_selected:
        is_more = False

    for vac in vacation_info:
        vacation_img = VacationPhoto.query.filter_by(vacation_id=vac.id).first()
        list_of_vacation.append([vac.name,
                                 vac.description,
                                 vacation_img.photo_name,
                                 is_more]
                                )

    return render_template('loggedVacations.html',
                           list_of_vacation=list_of_vacation,
                           user_name=session['name']
                           )
